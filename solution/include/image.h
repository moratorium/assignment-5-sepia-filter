#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


struct pixel {
    uint8_t b, g, r;
};

struct image {
    size_t width, height;
    struct pixel *data;
};

static inline size_t image_size(struct image image) { return image.width * image.height * sizeof(struct pixel); }

// Return new image with required width and height
struct image image_create(size_t width, size_t height);

// Free memory of image's data
void image_free(struct image *img);

// Return pointer to image_pixel_at by coordinates
static inline struct pixel *image_pixel_at(const struct image *img, const size_t x, const size_t y) { return img->data + img->width * y + x; }

// Pixels are equal
bool pixel_equal(struct pixel pixel1, struct pixel pixel2);

// Number of equal pixels (0, if different dimensions)
size_t image_equal_pixel_count(struct image image1, struct image image2);

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PATH
};

enum write_status {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_INVALID_PATH
};

// Method to read image of some format
typedef enum read_status (image_format_read)(FILE *, struct image *);

// Method to write image of some format
typedef enum write_status (image_format_write)(FILE *, struct image);

// Store methods to read from some format and write to some format
struct image_format {
    image_format_read *read;
    image_format_write *write;
};

// Read and store image from file by path, using method based by format
enum read_status image_read(const char *src, image_format_read *image_format_read, struct image *img);

// Write image form file by path, using method based by format
enum write_status image_write(const char *out, image_format_write *image_format_read, struct image img);

#endif //IMAGE_TRANSFORMER_IMAGE_H
