#include <malloc.h>
#include <stdlib.h>

#include "image.h"

struct image image_create(const size_t width, const size_t height) {
    struct pixel *data = malloc(width * height * sizeof(struct pixel));
    if (!data) {
        fprintf(stderr, "malloc: out of memory");
        abort();
    }
    return (struct image) {
            .width = width,
            .height = height,
            .data = data
    };
}

void image_free(struct image *img) {
    free(img->data);
}

bool pixel_equal(const struct pixel pixel1, const struct pixel pixel2) {
    return pixel1.r == pixel2.r
           && pixel1.b == pixel2.b
           && pixel1.g == pixel2.g;
}

size_t image_equal_pixel_count(struct image image1, struct image image2) {
    if (image1.height != image2.height
        || image1.width != image2.width) {
        return 0;
    }
    size_t res = 0;
    for (size_t i = 0; i < image1.height; i++) {
        for (size_t j = 0; j < image1.width; j++) {
            res += pixel_equal(*image_pixel_at(&image1, j, i), *image_pixel_at(&image2, j, i));
        }
    }
    return res;
}


enum read_status image_read(const char *restrict in, image_format_read *image_format_read, struct image *img) {
    FILE *file = fopen(in, "rb");
    if (!file) {
        return READ_INVALID_PATH;
    }
    const enum read_status status = image_format_read(file, img);
    fclose(file);
    return status;
}

enum write_status image_write(const char *restrict out, image_format_write *image_format_read, const struct image img) {
    FILE *file = fopen(out, "wb");
    if (!file) {
        return WRITE_INVALID_PATH;
    }
    const enum write_status status = image_format_read(file, img);
    fclose(file);
    return status;
}
