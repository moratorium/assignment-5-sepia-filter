#include <math.h>

#include "image_transform.h"


static struct image image_transform_color(struct image img, struct pixel (pixel_colorer)(struct pixel)) {
    const struct image blank = image_create(img.width, img.height);

    for (size_t i = 0; i < blank.height; i++) {
        for (size_t j = 0; j < blank.width; j++) {
            *image_pixel_at(&blank, j, i) = pixel_colorer(*image_pixel_at(&img, j, i));
        }
    }
    return blank;
}

struct color_ratio {
    float b, g, r;
};

struct pixel_color_ratio {
    struct color_ratio b, g, r;
};

static uint8_t color_ratio(const struct pixel p, struct color_ratio r) {
    const float color = (float) p.r * r.r + (float) p.g * r.g + (float) p.b * r.b;
    return (color>UINT8_MAX) ? UINT8_MAX : (uint8_t) lrintf(color);
}

static struct pixel pixel_color_ratio(const struct pixel p, const struct pixel_color_ratio ratio) {
    return (struct pixel) {
            .r = color_ratio(p, ratio.r),
            .g = color_ratio(p, ratio.g),
            .b = color_ratio(p, ratio.b)
    };
}

#define color_effect_ratio_single(ratio)    \
(struct pixel_color_ratio) {                \
    .b = {ratio, ratio, ratio},             \
    .g = {ratio, ratio, ratio},             \
    .r = {ratio, ratio, ratio}              \
}

extern void _image_transform_color_ratio_f(struct pixel *, struct pixel *, const float[], size_t);

static struct image image_transform_color_ratio_f(struct image img, struct pixel_color_ratio r) {
    const struct image blank = image_create(img.width, img.height);
    const float prepared_ratio[16] = {
            r.b.b, r.g.b, r.r.b, r.b.b,
            r.b.g, r.g.g, r.r.g, r.b.g,
            r.b.r, r.g.r, r.r.r, r.b.r,
    };

    _image_transform_color_ratio_f(img.data, blank.data, prepared_ratio, img.height * img.width);

    return blank;
}

static struct pixel pixel_color_inverse(struct pixel pixel) {
    return (struct pixel) {
            .g = UINT8_MAX - pixel.g,
            .r = UINT8_MAX - pixel.r,
            .b = UINT8_MAX - pixel.b
    };
}

struct image image_transform_color_inverse(struct image img) {
    return image_transform_color(img, pixel_color_inverse);
}

extern void _image_transform_color_inverse_f(struct pixel*, struct pixel*, size_t);

struct image image_transform_color_inverse_f(struct image img) {
    struct image blank = image_create(img.width, img.height);

    _image_transform_color_inverse_f(img.data, blank.data, img.height * img.width);

    return blank;
}

static struct pixel pixel_color_bw(struct pixel pixel) {
    const uint8_t mean = (uint8_t) roundf(((float) (pixel.b + pixel.g + pixel.r)) / 3);
    return (struct pixel) {
            .g = mean,
            .r = mean,
            .b = mean
    };
}

struct image image_transform_color_bw(struct image img) {
    return image_transform_color(img, pixel_color_bw);
}

struct image image_transform_color_bw_f(struct image img) {
    return image_transform_color_ratio_f(img, color_effect_ratio_single((float) 1/3));
}


#define SEPIA_RATIO                \
(struct pixel_color_ratio) {       \
    .r = {0.393F, 0.769F, 0.189F}, \
    .g = {0.349F, 0.686F, 0.168F}, \
    .b = {0.272F, 0.534F, 0.131F}  \
}

static struct pixel pixel_color_sepia(struct pixel pixel) {
    return pixel_color_ratio(pixel, SEPIA_RATIO);
}

struct image image_transform_color_sepia(struct image img) {
    return image_transform_color(img, pixel_color_sepia);
}

struct image image_transform_color_sepia_f(struct image img) {
    return image_transform_color_ratio_f(img, SEPIA_RATIO);
}

#undef SEPIA_RATIO