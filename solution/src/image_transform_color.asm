default rel
extern _GLOBAL_OFFSET_TABLE_

section .rodata
align 16
max_pixel_value: times 4 dd 255

%macro save 0
    pminsd xmm0, [rel max_pixel_value]

    pextrb [rsi],   xmm0,   0
    pextrb [rsi+1], xmm0,   4
    pextrb [rsi+2], xmm0,   8
    pextrb [rsi+3], xmm0,   12
%endmacro

%macro ratio_iteration 1
    pxor xmm0,xmm0
    pxor xmm1,xmm1
    pxor xmm2,xmm2

    pinsrb xmm0, [rdi],   0
    pinsrb xmm1, [rdi+1], 0
    pinsrb xmm2, [rdi+2], 0

    pinsrb xmm0, [rdi+3], 4
    pinsrb xmm1, [rdi+4], 4
    pinsrb xmm2, [rdi+5], 4

    shufps xmm0,xmm0, %1
    shufps xmm1,xmm1, %1
    shufps xmm2,xmm2, %1

    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5
    addps xmm0, xmm1
    addps xmm0, xmm2
    cvtps2dq xmm0, xmm0

    save

    add rdi, 3
    add rsi, 4
%endmacro

%macro prepare_ratio 1
    shufps xmm3,xmm3, %1
    shufps xmm4,xmm4, %1
    shufps xmm5,xmm5, %1
%endmacro

section .text
global _image_transform_color_ratio_f
_image_transform_color_ratio_f:

  .loop:
    ; moving from registers is faster than shufps
    movdqa xmm3, [rdx]      ; | c31, c21, c11, c31 |
    movdqa xmm4, [rdx+16]   ; | c32, c22, c12, c32 |
    movdqa xmm5, [rdx+32]   ; | c33, c23, c13, c33 |
                            ; |  B ,  G ,  R,  B |


    ;                                             | B , G,  R,  B  |
    ; | b1, b2, 0, 0 |                            | b1, b1, b1, b2 |
    ; | g1, g2, 0, 0 | -> mask (0 , 0 , 0 , 1) -> | g1, g1, g1, g2 |
    ; | r1, r2, 0, 0 |          00  00  00  01    | r1, r1, r1, r2 |
    ratio_iteration 0b01000000

    ; transform matrix mask
    ; | B,  G,  R, B | -> mask (1 , 2 , 0 , 1) -> | G,  R,  B,  G  |
    ;                           01  10  00  01
    prepare_ratio 0b01001001

    ;                                             | G,  R,  B,  G  |
    ; | b2, b3, 0, 0 |                            | b2, b2, b3, b3 |
    ; | g2, g3, 0, 0 | -> mask (0 , 0 , 1 , 1) -> | g2, g2, g3, g3 |
    ; | r2, r3, 0, 0 |          00  00  01  01    | r2, r2, r3, r3 |
    ratio_iteration 0b01010000

    ; transform matrix mask
    ; | G,  R,  B, G | -> mask (1 , 2 , 0 , 1) -> | R,  B,  G,  R  |
    ;                           01  10  00  01
    prepare_ratio 0b01001001

    ;                                             | R,  B,  G,  R  |
    ; | b3, b4, 0, 0 |                            | b3, b4, b4, b4 |
    ; | g3, g4, 0, 0 | -> mask (1 , 1 , 1 , 0) -> | g3, g4, g4, g4 |
    ; | r3, r4, 0, 0 |          00  01  01  01    | r3, r4, r4, r4 |
    ratio_iteration 0b01010100

    add rdi, 3

    sub rcx, 4
    cmp rcx, 0
    jg .loop

  ret

%macro inverse_iteration 0
    pxor xmm0, xmm0

    pinsrb xmm0, [rdi+0], 0
    pinsrb xmm0, [rdi+1], 4
    pinsrb xmm0, [rdi+2], 8
    pinsrb xmm0, [rdi+3], 12

    subps xmm0, xmm1

    save

    add rdi, 4
    add rsi, 4
%endmacro

global _image_transform_color_inverse_f
_image_transform_color_inverse_f:
   movdqa xmm1, [rel max_pixel_value]
  .loop:
    inverse_iteration
    inverse_iteration
    inverse_iteration

    sub rdx, 4
    cmp rdx, 0
    jg .loop

  ret