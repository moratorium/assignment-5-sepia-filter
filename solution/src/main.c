#include <time.h>
#include "image_bmp.h"
#include "image_transform.h"


int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    const char *in = argv[1];
    const char *out_c = argv[2];
    const char *out_asm = argv[3];

    struct image_format image_bmp = {
        .read = image_bmp_read,
        .write = image_bmp_write
    };

    struct image img;

    enum read_status read_status = image_read(in, image_bmp.read, &img);
    if (read_status != READ_OK) {
        return 1;
    }

    clock_t begin, end;

    fprintf(stderr, "Using standard method\n");
    begin = clock();
    struct image image_sepia = image_transform_color_sepia(img);
    end = clock();
    fprintf(stderr, "Took %ld ms to transform\n", (end - begin) * 1000 / CLOCKS_PER_SEC);




    fprintf(stderr, "Using fast method\n");
    begin = clock();
    struct image image_sepia_fast = image_transform_color_sepia_f(img);
    end = clock();
    fprintf(stderr, "Took %ld ms to transform\n", (end - begin) * 1000 / CLOCKS_PER_SEC);


    fprintf(stderr, "Equality: %f %% \n", (double) image_equal_pixel_count(image_sepia, image_sepia_fast) / ((double )img.width * img.height) * 100);

    image_free(&img);
    enum write_status write_status1 = image_write(out_c, image_bmp.write, image_sepia);
    image_free(&image_sepia);

    if (write_status1 != WRITE_OK) {
        return 2;
    }
    enum write_status write_status2 = image_write(out_asm, image_bmp.write, image_sepia_fast);
    image_free(&image_sepia_fast);

    if (write_status2 != WRITE_OK) {
        return 3;
    }


    return 0;
}
