#include <math.h>

#include "image.h"
#include "image_transform.h"


struct image image_transform_rotate(struct image img) {
    const struct image blank = image_create(img.height, img.width);

    for (size_t i = 0; i < blank.height; i++) {
        for (size_t j = 0; j < blank.width; j++)
            *image_pixel_at(&blank, j, i) = *image_pixel_at(&img, i, img.height - 1 - j);
    }
    return blank;
}

struct image image_transform_mirror_h(struct image img) {
    const struct image blank = image_create(img.width, img.height);

    for (size_t i = 0; i < blank.height; i++) {
        for (size_t j = 0; j < blank.width; j++)
            *image_pixel_at(&blank, j, i) = *image_pixel_at(&img, img.width - j, i);
    };
    return blank;
}

struct image image_transform_mirror_v(struct image img) {
    const struct image blank = image_create(img.width, img.height);

    for (size_t i = 0; i < blank.height; i++) {
        for (size_t j = 0; j < blank.width; j++)
            *image_pixel_at(&blank, j, i) = *image_pixel_at(&img, j, img.height - i - 1);
    }
    return blank;
}

bool image_transform_apply(image_transform effect, struct image_format image_format, const char *src, const char *out) {
    struct image img;

    enum read_status read_status = image_read(src, image_format.read, &img);
    if (read_status != READ_OK) {
        return false;
    };

    struct image transformed_image = effect(img);
    image_free(&img);

    enum write_status write_status = image_write(out, image_format.write, transformed_image);
    image_free(&transformed_image);

    if (write_status != WRITE_OK) {
        return false;
    }
    return true;
}

