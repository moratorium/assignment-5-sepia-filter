# Test 1 (1518x941 IMAGE)
```asm
Using standard method
Took 5 ms to transform
Using fast method
Took 4 ms to transform
Equality: 99.998320 % 
```
# Test 2 (1280x720 IMAGE)
```asm
Using standard method
Took 3 ms to transform
Using fast method
Took 2 ms to transform
Equality: 99.978841 %
```

# Test 3 (4928x3280 IMAGE)
```asm
Using standard method
Took 63 ms to transform
Using fast method
Took 50 ms to transform
Equality: 99.987540 % 
```

